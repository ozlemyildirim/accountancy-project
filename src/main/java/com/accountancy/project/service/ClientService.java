package com.accountancy.project.service;

import com.accountancy.project.dao.AccountDAO;
import com.accountancy.project.dao.ClientDAO;
import com.accountancy.project.dao.TransactionFollowDAO;
import com.accountancy.project.dto.AccountDTO;
import com.accountancy.project.dto.ClientDTO;
import com.accountancy.project.dto.TransactionFollowDTO;
import com.accountancy.project.entity.Account;
import com.accountancy.project.entity.BalanceStatus;
import com.accountancy.project.entity.Client;
import com.accountancy.project.entity.TransactionFollow;
import com.accountancy.project.mapper.AccountMapper;
import com.accountancy.project.mapper.ClientMapper;
import com.accountancy.project.mapper.TransactionFollowMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ClientService {


    private final AccountDAO accountDAO;
    private final ClientDAO clientDAO;
    private final TransactionFollowDAO transactionFollowDAO;
    private final ClientMapper clientMapper = Mappers.getMapper(ClientMapper.class);
    ;
    private final AccountMapper accountMapper = Mappers.getMapper(AccountMapper.class);
    private final TransactionFollowMapper transactionFollowMapper = Mappers.getMapper(TransactionFollowMapper.class);

    @Autowired
    public ClientService(AccountDAO accountDAO, ClientDAO clientDAO, TransactionFollowDAO transactionFollowDAO) {
        this.accountDAO = accountDAO;
        this.clientDAO = clientDAO;
        this.transactionFollowDAO = transactionFollowDAO;
    }

    /**
     *
     * @param clientDTO
     * @return
     */
    public ClientDTO save(ClientDTO clientDTO) {
        return clientMapper.toDto(clientDAO.save(clientMapper.toEntity(clientDTO)));
    }

    /**
     *
     * @param accountDTO
     * @param id
     * @return
     */
    public AccountDTO saveAccount(AccountDTO accountDTO, Long id) {
        if (clientDAO.findById(id).isPresent())
            accountDTO.setClient(clientMapper.toDto(clientDAO.findById(id).get()));
        return accountMapper.toDto(accountDAO.save(accountMapper.toEntity(accountDTO)));
    }

    /**
     *
     * @param id
     * @return
     */
    public List<AccountDTO> findClientAccount(Long id) {
        List<Account> accountList = accountDAO.findAllByClient_Id(id);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        for (Account account : accountList)
            accountDTOS.add(accountMapper.toDto(account));
        return accountDTOS;
    }

    /**
     *
     * @param id
     * @return
     */
    public List<TransactionFollowDTO> findAllByCreditAccount_IdOrDebitAccount_id(Long id) {
        List<TransactionFollow> transactionFollows = transactionFollowDAO.findAllByCreditAccount_IdOrDebitAccount_id(id, id);
        List<TransactionFollowDTO> transactionFollowDTOS = new ArrayList<>();
        for (TransactionFollow transactionFollow : transactionFollows)
            transactionFollowDTOS.add(transactionFollowMapper.toDto(transactionFollow));
        return transactionFollowDTOS;
    }

    /**
     *
     * @param id
     * @return
     */
    public ClientDTO findById(Long id) {
        if (clientDAO.findById(id).isPresent())
            return clientMapper.toDto(clientDAO.findById(id).get());
        else
            return new ClientDTO();
    }

    /**
     *
     * @return
     */
    public List<ClientDTO> findAll() {
        List<Client> clients = clientDAO.findAll();
        List<ClientDTO> clientDTOS = new ArrayList<>();
        for (Client c : clients)
            clientDTOS.add(clientMapper.toDto(c));
        return clientDTOS;
    }

    /**
     *
     * @param pId
     * @param nId
     * @param balance
     * @return
     */
    public TransactionFollowDTO transfer(Long pId, Long nId, Double balance) {
        TransactionFollowDTO transactionFollowDTO = new TransactionFollowDTO();
        AccountDTO paccountDTO = null, naccountDTO = null;
        if (accountDAO.findById(pId).isPresent())
            paccountDTO = accountMapper.toDto(accountDAO.findById(pId).get());
        if (accountDAO.findById(nId).isPresent())
            naccountDTO = accountMapper.toDto(accountDAO.findById(nId).get());
        if (paccountDTO != null && naccountDTO != null)
            if (paccountDTO.getBalanceStatus().equals(BalanceStatus.DR) && naccountDTO.getBalanceStatus().equals(BalanceStatus.CR)) {
                paccountDTO.setBalance(paccountDTO.getBalance() - balance);
                naccountDTO.setBalance(naccountDTO.getBalance() + balance);
                transactionFollowDTO.setCreditAccount(paccountDTO);
                transactionFollowDTO.setDebitAccount(naccountDTO);
                transactionFollowDTO.setAmount(balance);
                transactionFollowDTO.setMessage("");
                transactionFollowDTO.setDateCreated(new Date());
                accountDAO.save(accountMapper.toEntity(paccountDTO));
                accountDAO.save(accountMapper.toEntity(naccountDTO));
                transactionFollowDAO.save(transactionFollowMapper.toEntity(transactionFollowDTO));
            }
        return transactionFollowDTO;
    }
}
