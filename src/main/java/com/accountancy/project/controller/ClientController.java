package com.accountancy.project.controller;

import com.accountancy.project.dto.AccountDTO;
import com.accountancy.project.dto.ClientDTO;
import com.accountancy.project.dto.TransactionFollowDTO;
import com.accountancy.project.entity.Client;
import com.accountancy.project.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/***
 * @author ozlemyildirim
 */
@RestController
@RequestMapping(value = "client")
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    /**
     *
     * @param clientDTO
     * @return
     */
    @PostMapping(value = "/save")
    public ResponseEntity<ClientDTO> save(@RequestBody ClientDTO clientDTO) {
        return new ResponseEntity<>(clientService.save(clientDTO), HttpStatus.OK);
    }

    /**
     *
     * @param id
     * @param accountDTO
     * @return
     */
    @PostMapping(value = "/saveAccount/{id}")
    public ResponseEntity<AccountDTO> save(@PathVariable(value = "id") Long id,@RequestBody AccountDTO accountDTO) {
        return new ResponseEntity<>(clientService.saveAccount(accountDTO, id), HttpStatus.OK);
    }

    /**
     *
     * @param pId
     * @param nId
     * @param balance
     * @return
     */
    @PostMapping(value = "/transfer/{pId}/{nId}/{balance}")
    public ResponseEntity<TransactionFollowDTO> transfer(@PathVariable(value = "pId") Long pId,@PathVariable(value = "nId") Long nId,@PathVariable(value = "balance") Double balance) {
        return new ResponseEntity<>(clientService.transfer(pId,nId,balance), HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @GetMapping(value = "/findAll")
    public ResponseEntity<List<ClientDTO>> findAll() {
        return new ResponseEntity<>(clientService.findAll(), HttpStatus.OK);
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/findById/{id}")
    public ResponseEntity<ClientDTO> findById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(clientService.findById(id), HttpStatus.OK);
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/findClientAccount/{id}")
    public ResponseEntity<List<AccountDTO>> findClientAccount(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(clientService.findClientAccount(id), HttpStatus.OK);
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/findAllByCreditAccount_IdOrDebitAccount_id/{id}")
    public ResponseEntity<List<TransactionFollowDTO>> findAllByCreditAccount_IdOrDebitAccount_id(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(clientService.findAllByCreditAccount_IdOrDebitAccount_id(id), HttpStatus.OK);
    }

}
