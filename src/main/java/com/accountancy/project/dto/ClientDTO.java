package com.accountancy.project.dto;

import lombok.Data;

@Data
public class ClientDTO extends BaseDTO {

    private String name;

    private String surname;

    private String primaryAddress;

    private String secondaryAddress;


}
