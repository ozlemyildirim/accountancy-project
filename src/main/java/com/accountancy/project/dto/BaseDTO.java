package com.accountancy.project.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseDTO implements Serializable {
    private Long id;
}
