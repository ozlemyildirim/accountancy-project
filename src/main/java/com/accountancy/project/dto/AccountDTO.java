package com.accountancy.project.dto;

import com.accountancy.project.entity.BalanceStatus;
import com.accountancy.project.entity.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AccountDTO extends BaseDTO implements Serializable {
    private ClientDTO client;

    private Type type;

    private BalanceStatus balanceStatus;

    private Double balance;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date dateCreated;
}
