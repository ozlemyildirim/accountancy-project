package com.accountancy.project.dto;

import com.accountancy.project.entity.Account;
import com.accountancy.project.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Data
public class TransactionFollowDTO extends BaseDTO {
    private AccountDTO debitAccount;

    private AccountDTO creditAccount;

    private Double amount;

    private String message;

    private Date dateCreated;
}
