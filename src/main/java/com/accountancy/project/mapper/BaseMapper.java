package com.accountancy.project.mapper;

public interface  BaseMapper<T,K>{

    T toEntity(K dto);

    K toDto(T entity);

}