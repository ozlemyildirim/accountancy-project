package com.accountancy.project.mapper;

import com.accountancy.project.dto.ClientDTO;
import com.accountancy.project.entity.Client;
import org.mapstruct.Mapper;

@Mapper
public interface ClientMapper extends BaseMapper<Client, ClientDTO> {
}
