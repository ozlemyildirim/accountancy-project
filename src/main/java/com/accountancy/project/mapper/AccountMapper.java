package com.accountancy.project.mapper;

import com.accountancy.project.dto.AccountDTO;
import com.accountancy.project.entity.Account;
import org.mapstruct.Mapper;

@Mapper
public interface AccountMapper extends BaseMapper<Account,AccountDTO> {
}
