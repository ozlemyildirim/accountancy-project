package com.accountancy.project.mapper;

import com.accountancy.project.dto.TransactionFollowDTO;
import com.accountancy.project.entity.Account;
import com.accountancy.project.entity.BaseEntity;
import com.accountancy.project.entity.TransactionFollow;
import lombok.Data;
import org.mapstruct.Mapper;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Mapper
public interface TransactionFollowMapper extends BaseMapper<TransactionFollow,TransactionFollowDTO> {
}
