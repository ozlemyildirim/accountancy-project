package com.accountancy.project.dao;


import com.accountancy.project.entity.BaseEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseDAO<T extends BaseEntity> extends JpaRepository<T,Long> {
    List<T> findAll();
    Optional<T> findById(Long id);
}
