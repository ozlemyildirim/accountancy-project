package com.accountancy.project.dao;

import com.accountancy.project.entity.Client;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientDAO extends BaseDAO<Client> {
}
