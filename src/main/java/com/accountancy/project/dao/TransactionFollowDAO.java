package com.accountancy.project.dao;

import com.accountancy.project.entity.TransactionFollow;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionFollowDAO extends BaseDAO<TransactionFollow> {
    List<TransactionFollow> findAllByCreditAccount_IdOrDebitAccount_id(Long cId,Long dId);
}
