package com.accountancy.project.dao;

import com.accountancy.project.entity.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDAO extends BaseDAO<Account> {
    List<Account> findAllByClient_Id(Long id);
}
