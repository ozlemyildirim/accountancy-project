package com.accountancy.project.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

/***
 * @author ozlemyildirim
 */
@Entity
@Data
@Table(name = "ACCOUNT")
public class Account extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Enumerated(EnumType.STRING)
    private BalanceStatus balanceStatus;

    private Double balance;

    private Date dateCreated;
}
