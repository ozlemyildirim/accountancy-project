package com.accountancy.project.entity;

/***
 * @author ozlemyildirim
 *
 * enum class for Balance Status
 */
public enum BalanceStatus {
    DR,
    CR
}
