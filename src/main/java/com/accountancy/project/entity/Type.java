package com.accountancy.project.entity;

/***
 * @author ozlemyildirim
 *
 * enum class
 */
public enum Type {
    CURRENT,
    SAVINGS
}
