package com.accountancy.project.entity;


/***
 * @author ozlemyildirim
 */

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "TRANSACTION_FOLLOW")
public class TransactionFollow extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "debit_account_id")
    private Account debitAccount;

    @ManyToOne
    @JoinColumn(name = "credit_account_id")
    private Account creditAccount;

    private Double amount;

    private String message;

    private Date dateCreated;
}
