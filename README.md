###Accountancy Project
This project contains some accounting processes.It allows you to create new clients,listing all clients and their details.You can create client account and you can get list of client accounts.You can make transfer depends on accounts which are called transactions.

#####Installations
You should have PostgreSql to run the project and see the datas & tables.You may download it from the link below:

[Download PostgreSQL 10.11](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)

-You must set username='postgres' and password='123456'.(You can find these configuration properties on application.properties file in project -> [application.properties](https://gitlab.com/ozlemyildirim/accountancy-project/blob/master/src/main/resources/application.properties))
- After you installed it,please create a database by name 'accountancy' on pgAdmin.
- Therefore,when you run the project the tables will be created automatically.
- There is no need of adding Tomcat because SpringBoot has been used.Project will be run directly after click run button.

#####Running the tests
*You may import the 'Accountancy.postman_collection.json' file on POSTMAN.* There are some written data sets for test.
-  By that import; you will see collection of project under the name of 'Accountancy'.And you will find requests in this collection set.By 'SEND' button the related method will be working for datas written in file.
-  *To test transfer process*; you should create CR and DR accounts first.After that you must be sure that the ids' on your transfer post URL match them.(For example:localhost:8080/client/transfer/3/2/50 In this URL the first id(3) represents DR account's id and the second one(2) represents CR account's id which you created before transfer process(You can find these accounts' ids' in the 'account' table on db.).*You may see these URL paths in code as well.*

#####Deployment
There is no need of deployment to see how the project works.

#####Built With
- Maven
- Spring MVC
- Spring Boot
- Hibernate – Spring Data JPA
- MapStruct
- PostgreSQL
- GitLab – as source code management

#####Built By
 Özlem Yıldırım 

#####Acknowledgements
The project is based on the task instructions which was forwarded to me by Openpayd Team. 


